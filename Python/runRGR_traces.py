#!/usr/bin/env python
import sys
# sys.path.append('/home/philipe/.pyenv/versions/3.5.1/lib/python3.5/site-packages')
sys.path.append('/usr/local/lib/python3.5/dist-packages')

from distutils.core import setup, Extension

import cv2 as cv
import numpy as np

import scipy.io as sio
import numpy.matlib as npm

# imports for calling C++ reg.growing code
import ctypes
import callRGR

# for parallelism
import multiprocessing

#####

def regGrowing(area,numSamples,R_H,height,width,sz,preSeg,m,img_r,img_g,img_b,clsMap,numCls,return_dict,itSet):
    # h is the index o pixels p_h within R_H. We randomly sample seeds
    # according to h~U(1,|R_H|)
    # round down + Uniform distribution
    h = np.floor(area * np.random.random((area,)))
    h = h.astype(np.int64)
 
    # s is the index of each seed for current region growing step
    # sequence
    idSeeds = np.arange(0,numSamples) # IDs of random seeds
    idSeeds = idSeeds.astype(np.int64)

    posSeeds = h[idSeeds] # get the position of these seeds within R_H

    # S is the corresponding set of all seeds, mapped into
    # corresponding img-size matrix
    s = R_H[posSeeds]        
    S = np.zeros((height, width))
    S[np.unravel_index(s, S.shape, 'F')] = 1  

    # allocate memory for output returned by reg.growing C++ code
    RGRout = np.zeros((width*height), dtype=int)

    S = S.flatten(order='F')

    # call reg.growing code (adapted SNIC) in C++, using Cython (see callRGR.pyx and setup.py)
    # perform region growing. PsiMap is the output map of generated
    out_ = callRGR.callRGR(img_r, img_g, img_b, preSeg.astype(np.int32), S.astype(np.int32), width, height, numSamples, m,RGRout.astype(np.int32))
    PsiMap = np.asarray(out_)       

    # number of generated clusters.  We subtract 2 to disconsider the pixels pre-classified as background (indexes -1 and 0)
    N = np.amax(PsiMap)-2

    clsScores = clsMap.flatten(order='F')
    clsScores = clsScores.astype(np.double)

    # majority voting per cluster
    for k in range(0, N):       
        p_j_ = np.nonzero(PsiMap == k)
        p_j_ = np.asarray(p_j_)

        for itCls in range(0, numCls):
            idxOffset = sz*itCls;
            p_j_cls = p_j_ + idxOffset;

            noPositives =  (np.count_nonzero(clsScores[p_j_cls] > 0));
            clsScores[p_j_cls] = float(noPositives)/p_j_.size

    clsScores = np.reshape(clsScores,(height,width,numCls),order='F')    

    return_dict[itSet] = clsScores

########
# anns: annotation or pre-segmentation to be refined
def RGR(img,anns,m,numSets,cellSize):

    # get image size, basically height and width
    height, width, channels = img.shape
    heightAnns, widthAnns = anns.shape

    if(widthAnns != width):
        img = cv.resize(img, (widthAnns, heightAnns)) 

    height, width, channels = img.shape

    # flattening (i.e. vectorizing) matrices to pass it to C++ function (** OPENCV LOADS BGR RATHER THAN RGB!)
    img_b = img[:,:,0].flatten() # R channel
    img_g = img[:,:,1].flatten() # G channel
    img_r = img[:,:,2].flatten() # B channel

    img_b = img_b.astype(np.int32)
    img_g = img_g.astype(np.int32)
    img_r = img_r.astype(np.int32)

    # image size 
    sz = width*height

    # load PASCAL colormap in CV format
    lut = np.load('PASCALlutW.npy')

    # Rectangular Kernel - equal to strel in matlab
    SE = cv.getStructuringElement(cv.MORPH_RECT, (80, 80))  # used for identifying far background

    # RGR - refine each class
    # list of annotated classes
    clsList = np.unique(anns)
    clsList = np.delete(clsList,0) # remove class 0 
    numCls = clsList.size # number of classes

    # annotations masks per class
    clsMap = np.zeros((height,width,numCls))
    for itCls in range(0, numCls):
        np.putmask(clsMap[:,:,itCls],anns == clsList[itCls],1) 

    # mask of annotated pixels: 
    # in this case, only annotated traces are high-confidence (index 2),
    # all others are uncertain (index 0)
    preSeg = np.int32(np.zeros((height,width)))
    np.putmask(preSeg,anns > 0,2)
    RoI = preSeg

    # identify all high confidence pixels composing the RoI
    area = np.count_nonzero(RoI)

    # R_H is the high confidence region, the union of R_nB and R_F
    R_H = np.nonzero(RoI.flatten('F') > 0)
    R_H = R_H[0]

    # number of seeds to be sampled is defined by the ratio between
    # |R_H| and desired spacing between seeds (cellSize)
    # round up
    numSamples = np.ceil(area / cellSize)

    preSeg = preSeg.flatten()

    # matrix that will contain the scoremaps for each iteration
    # ref_cls = np.zeros((height, width, numCls, numSets),dtype=float)    
    ref_cls = np.zeros((height*width*numCls, numSets),dtype=float)    

    num_cores = multiprocessing.cpu_count()

    manager = multiprocessing.Manager()
    return_dict = manager.dict()

    jobs = []
    for itSet in range(0, numSets):
        p = multiprocessing.Process(target=regGrowing, args=(area,numSamples,R_H,height,width,sz,preSeg,m,img_r,img_g,img_b,clsMap,numCls,return_dict,itSet))
        jobs.append(p)
        p.start()

    for proc in jobs:
        proc.join()

    outputPar = return_dict.values()    

    outputPar = np.asarray(outputPar)

    # swapping axes, because parallel returns (numSets,...)
    ref_cls = np.moveaxis(outputPar,0,3)

    # averaging scores obtained for each set of seeds
    ref_M = (np.sum(ref_cls,axis=3))/numSets        

    # maximum likelihood across refined classes scores ref_M
    maxScores = np.amax(ref_M,axis=2)
    maxClasses = np.argmax(ref_M,axis=2)

    detMask = np.uint8(maxClasses+1)

    finalMask = np.zeros((height,width),dtype=float);    
    for itCls in range(0, numCls):       
       np.putmask(finalMask,detMask == itCls+1,clsList[itCls]) 

    finalMask = np.uint8(finalMask-1)

    # sio.savemat('intermediate.mat', mdict={'anns':anns,'ref_M': ref_M,'ref_cls':ref_cls,'finalMaskRGR':finalMask})  
    # apply colormap
    _,alpha = cv.threshold(finalMask,0,255,cv.THRESH_BINARY)

    finalMask = cv.cvtColor(np.uint8(finalMask), cv.COLOR_GRAY2RGB)    
    im_color = cv.LUT(finalMask, lut)    

    b, g, r = cv.split(im_color)
    rgba = [b,g,r, alpha]
    im_color = cv.merge(rgba,4) 

    return im_color

def main():
    imgfile = '../Example/2010_001939.jpg';
    annsfile = '../Example/annsEx.mat';

    ## RGR parameters
    # fixed parameters
    m = .1  # theta_m: balance between
    numSets = 8    # number of seeds sets (samplings)
    cellSize = 1.333   # average spacing between samples

    img = cv.imread(imgfile)

    # load scores.mat file
    annsmat = sio.loadmat(annsfile)
    anns = annsmat['anns']

    im_color = RGR(img,anns,m,numSets,cellSize)

    cv.imwrite('refined.png', im_color)

    # just for displaying inputs and outputs
    # load PASCAL colormap in CV format
    lut = np.load('PASCALlutW.npy')

    _,alpha = cv.threshold(np.uint8(anns-1),0,255,cv.THRESH_BINARY)

    annsMask = cv.cvtColor(np.uint8(anns-1), cv.COLOR_GRAY2RGB)    
    anns_color = cv.LUT(annsMask, lut)    

    b, g, r = cv.split(anns_color)
    rgba = [b,g,r, alpha]
    anns_color = cv.merge(rgba,4) 

    cv.imshow('1', img)    ## *255 because value range should be
    cv.imshow('2', anns_color)    ## *255 because value range should be
    cv.imshow('3', im_color)    ## *255 because value range should be
    cv.waitKey(0) & 0xFF

if __name__== "__main__":
    main()

