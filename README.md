### Public release of Region Growing Refinement (RGR) algorithm 
## Available under the Non-Profit Open Software License: for more details https://opensource.org/licenses/NPOSL-3.0 . 
Please also note that the region growing .cpp file composing RGR is an adaptation of the SNIC algorithm, so copyright/license details of SNIC also apply (see .cpp file por details)

Both MATLAB and Python versions 

# MATLAB
Example uses RGR to refine semantic segmentation predicted by a state-of-the-art CNN

# Python 
Example propagates scribbles provided by an user for annotation of image segmentation masks. See our FreeLabel interface for more details: https://coviss.org/freelabel

## Download, configuration and deploying:
1. clone repository
2. cd rgr-public/Python
3. create virtual environment: virtualenv . (if you have multiple python versions, run: virtualenv -p python3 .)
4. enter virtual environment: source ./bin/activate
5. install requirements: pip install -r requirements.txt (if it fails, try upgrading pip: pip install --upgrade pip)
6. recompile "callRGR", which compiles .cpp file for Python usage: 
	- python setup.py build_ext --inplace
7. run the .py code of your choice: 
	- Refine CNN coarse segmentation: python runRGR.py 
	- Grow segmentation mask from traces: python runRGR_traces.py

For more details on RGR, check the manuscript available as .pdf in this repository or at https://coviss.org/publications

